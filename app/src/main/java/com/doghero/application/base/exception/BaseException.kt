package com.doghero.application.base.exception

/**
 * Base Class for handling errors/failures/exceptions.
 * Every feature specific should extend [BusinessException] class.
 */
sealed class BaseException(message: String = "") : RuntimeException(message)

/**
 * Extend this class for feature specific failures.
 *
 * */
abstract class BusinessException : BaseException()

/**
 * Exception thrown when an essential parameter is missing in the
 * backend/network response.
 *
 */
class EssentialParamMissingException(
    missingParam: String,
    rawObject: Any
) : BaseException("The params: $missingParam are missing in received object: $rawObject")