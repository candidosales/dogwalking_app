package com.doghero.application.di

import com.doghero.application.base.common.utils.CurrencyUtils
import org.koin.dsl.module.module

val utilsModule = module {
    single { CurrencyUtils() }
}