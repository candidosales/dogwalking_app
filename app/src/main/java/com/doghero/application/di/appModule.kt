package com.doghero.application.di

import com.doghero.application.heroes.data.HeroesRepository
import com.doghero.application.heroes.presentation.HeroAdapter
import com.doghero.application.heroes.presentation.HeroesFragment
import com.doghero.application.heroes.presentation.HeroesViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel

import org.koin.dsl.module.module


val appModule = module {
    factory { HeroesFragment() }
    factory { HeroesRepository(get()) }
    viewModel { HeroesViewModel(get()) }
}
