package com.doghero.application.heroes.presentation

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.Glide
import com.doghero.application.R
import com.doghero.application.base.common.utils.CurrencyUtils
import com.doghero.application.base.extensions.hideOrShow
import com.doghero.application.heroes.model.Hero
import kotlinx.android.synthetic.main.item_hero.view.*


class HeroAdapter(private val currencyUtils: CurrencyUtils, private val showHeart: Boolean = false) : RecyclerView.Adapter<HeroAdapter.HeroViewHolder>() {

    private var heroes: List<Hero>? = null

    fun setDataSource(dataSource: List<Hero>) {
        this.heroes = dataSource
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = heroes?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_hero, parent, false)
        return HeroViewHolder(view, showHeart)
    }

    override fun onBindViewHolder(holder: HeroViewHolder, position: Int) {
        val heroView = mapper(heroes!![position])

        holder.bind(heroView)

        holder.itemView.heartImage.setOnClickListener {
            holder.startAnimation()
        }

        holder.itemView.heartAnimation.setOnClickListener {
            holder.resumeAnimation()
        }
    }

    private fun mapper(hero: Hero) : HeroView {
        val priceWithoutSymbol = currencyUtils.normalizeAmountWithoutSymbol(hero.price)

        return HeroView(
            name = hero.user.first_name,
            priceFormatted = "${currencyUtils.getCurrencySymbol()} <b>$priceWithoutSymbol</b>",
            neighborhood = hero.address_neighborhood,
            imageUrl =  hero.user.image_url,
            isSuperHero = hero.is_superhero
        )
    }


    class HeroViewHolder(val view: View, private val showHeart: Boolean) : ViewHolder(view) {

        private val heroUserFirstName: TextView = view.findViewById(R.id.heroUserFirstName)
        private val heroAddressNeighborhood: TextView = view.findViewById(R.id.heroAddressNeighborhood)
        private val heroPrice: TextView = view.findViewById(R.id.heroPrice)
        private val heroUserImage: ImageView = view.findViewById(R.id.heroUserImage)
        private val heroSuperHero: ImageView = view.findViewById(R.id.heroSuperHero)

        private val heartImage: ImageView = view.findViewById(R.id.heartImage)
        private val heartAnimation: LottieAnimationView = view.findViewById(R.id.heartAnimation)

        fun bind(heroView: HeroView) {
            heroUserFirstName.text = heroView.name
            heroAddressNeighborhood.text = heroView.neighborhood
            heroPrice.text = Html.fromHtml(heroView.priceFormatted)
            heroSuperHero.hideOrShow(heroView.isSuperHero)

            Glide.with(view.context).load(heroView.imageUrl).into(heroUserImage)

            heartImage.hideOrShow(showHeart)
        }

        fun startAnimation() {
            heartImage.visibility = View.INVISIBLE
            heartAnimation.visibility = View.VISIBLE
            heartAnimation.playAnimation()

        }

        fun resumeAnimation() {
            heartAnimation.progress = 0.0f
            heartAnimation.cancelAnimation()
            heartAnimation.visibility = View.INVISIBLE
            heartImage.visibility = View.VISIBLE
        }

    }
}