package com.doghero.application.heroes.model

data class User(
    val first_name: String = "",
    val image_url: String = ""
)