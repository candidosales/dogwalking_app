package com.doghero.application.heroes.data

import com.doghero.application.base.data.ApiResponse
import com.doghero.application.heroes.model.Heroes


class HeroesRepository(
    private val heroService: HeroService) {

    suspend fun getHeroes(): ApiResponse<Heroes> {

        val apiResponse = heroService.getHeroes().await()

        val heroesMapper = HeroesMapper()
        return if (apiResponse.isSuccessful)
            ApiResponse.Success(heroesMapper.apply(apiResponse.body()!!))
        else ApiResponse.Error(apiResponse.message(), apiResponse.code())
    }
}