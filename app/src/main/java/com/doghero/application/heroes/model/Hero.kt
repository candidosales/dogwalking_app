package com.doghero.application.heroes.model

data class Hero(
    val is_superhero: Boolean,
    val user: User,
    val address_neighborhood: String,
    val price: Long
)