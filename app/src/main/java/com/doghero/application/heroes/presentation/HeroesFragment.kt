package com.doghero.application.heroes.presentation

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.doghero.application.R
import com.doghero.application.base.presentation.BaseFragment
import com.doghero.application.heroes.model.Heroes
import kotlinx.android.synthetic.main.fragment_heroes.*
import androidx.lifecycle.Observer
import com.doghero.application.base.common.utils.CurrencyUtils
import kotlinx.android.synthetic.main.fragment_heroes.view.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class HeroesFragment : BaseFragment() {

    override val layoutId: Int
        get() = R.layout.fragment_heroes

    private val viewModel: HeroesViewModel by viewModel()
    private val currencyUtils: CurrencyUtils by inject()

    private lateinit var heroes: Heroes

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getHeroes()
    }

    private fun getHeroes() {
        toggleLoading(true)
        viewModel.heroes.observe(this, Observer {
            toggleLoading(false)

            heroes = it
            updateAdapter()
        })
    }

    private fun toggleLoading(toggle: Boolean) {
        loading.visibility = if (toggle) View.VISIBLE else View.GONE
        nestedScroll.visibility = if (toggle) View.GONE else View.VISIBLE
    }

    private fun updateAdapter() {
        if (!::heroes.isInitialized) {
            return
        }

        val layoutManagerRecents = LinearLayoutManager(context)
        val adapterHeroesRecents = HeroAdapter(currencyUtils)
        heroesRecentsRecyclerView.adapter = adapterHeroesRecents
        heroesRecentsRecyclerView.layoutManager = layoutManagerRecents

        adapterHeroesRecents.setDataSource(heroes.recents)


        val layoutManagerFavorites = LinearLayoutManager(context)
        val adapterHeroesFavories = HeroAdapter(currencyUtils, showHeart = true)
        heroesFavoritesRecyclerView.adapter = adapterHeroesFavories
        heroesFavoritesRecyclerView.layoutManager = layoutManagerFavorites

        adapterHeroesFavories.setDataSource(heroes.favorites)

    }
}