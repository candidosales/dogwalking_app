package com.doghero.application.heroes.presentation

data class HeroView (
    val name: String,
    val priceFormatted: String,
    val neighborhood: String,
    val imageUrl: String,
    val isSuperHero: Boolean
)