package com.doghero.application.heroes.model

data class Heroes(
    val recents: List<Hero>,
    val favorites: List<Hero>
)