package com.doghero.application.heroes.data

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET

interface HeroService {

    @GET("5bda5f6a51e8b664f2c60ead")
    fun getHeroes(): Deferred<Response<HeroesResponseRaw>>

}
