package com.doghero.application.heroes.data

import com.doghero.application.base.BaseMapper
import com.doghero.application.heroes.model.Hero
import com.doghero.application.heroes.model.Heroes

class HeroesMapper : BaseMapper<HeroesResponseRaw, Heroes>() {

    override fun checkParams(raw: HeroesResponseRaw, missingFields: MutableList<String>) {
        if (raw.recents == null) {
            missingFields.add("recents")
        }

        if (raw.favorites == null) {
            missingFields.add("favorites")
        }
    }

    override fun copyValues(raw: HeroesResponseRaw): Heroes {
        val recents = if (raw.recents.isEmpty()) {
            emptyList()
        } else {
            raw.recents.map { hero ->
                Hero(
                    is_superhero = hero.is_superhero,
                    user = hero.user,
                    address_neighborhood = hero.address_neighborhood,
                    price = hero.price
                )
            }
        }

        val favorites = if (raw.favorites.isEmpty()) {
            emptyList()
        } else {
            raw.favorites.map { hero ->
                Hero(
                    is_superhero = hero.is_superhero,
                    user = hero.user,
                    address_neighborhood = hero.address_neighborhood,
                    price = hero.price
                )
            }
        }

        return Heroes(
            favorites = favorites,
            recents = recents
        )
    }

}