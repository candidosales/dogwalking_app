package com.doghero.application.heroes.data

import com.doghero.application.heroes.model.Hero
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HeroesResponseRaw(
    @Json(name = "favorites") val favorites: List<Hero>,
    @Json(name = "recents") val recents: List<Hero>
)