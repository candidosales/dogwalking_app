package com.doghero.application.heroes.presentation

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.doghero.application.base.data.ApiResponse
import com.doghero.application.base.presentation.ScopedViewModel
import com.doghero.application.heroes.data.HeroesRepository
import com.doghero.application.heroes.model.Heroes
import kotlinx.coroutines.*
import timber.log.Timber

class HeroesViewModel (private val heroesRepository: HeroesRepository) : ScopedViewModel() {

    var heroes: MutableLiveData<Heroes> = MutableLiveData()

    init {
        scope.launch {
            withContext(Dispatchers.Default) {
                fetchHeroes()
            }
        }

    }


    private fun fetchHeroes() {
        CoroutineScope(Dispatchers.IO).launch {

            val response = heroesRepository.getHeroes()

            when(response) {

                is ApiResponse.Success -> heroes.postValue(response.body)

                //TODO: Create UI element to handle response error
                is ApiResponse.Error -> {
                    Timber.d("fetchHeroes: ResponseError. " +
                            "\n Message -> ${response.errorMessage}" +
                            "\n ErrorCode -> ${response.errorCode}")
                }
            }
        }
    }
}