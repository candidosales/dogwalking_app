package com.doghero.application

import android.app.Application
import com.doghero.application.di.appModule
import com.doghero.application.di.networkModule
import com.doghero.application.di.utilsModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber

open class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()

        startKoin(this, listOf(appModule, networkModule, utilsModule))

    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}