package com.doghero.application

import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import com.doghero.application.base.presentation.BaseActivity
import com.doghero.application.heroes.presentation.HeroesFragment
import org.koin.android.ext.android.inject

class MainActivity : BaseActivity() {

    override val layoutId: Int
        get() = R.layout.activity_main

    private val heroesFragment: HeroesFragment by inject()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar.setTitle(R.string.app_name)

        if (savedInstanceState == null)
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, heroesFragment)
                .commitAllowingStateLoss()
    }
}

