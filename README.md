# Doghero Test - Android


![Apresentação](https://bitbucket.org/candidosales/dogwalking_app/raw/b7dc8b5ebb87e85f5ccbd90a4b85cf9d33bf3d1f/presentation-resize.gif)

## Considerações

A arquitetura adotada para este projeto foi o MVVM, Architecture Components e Clean Architecture, usando a injeção de dependência via Koin.


## Linguagens, Bibliotecas e ferramentas usadas

* [Kotlin](https://kotlinlang.org/)
* [Android Architecture Components](https://developer.android.com/topic/libraries/architecture/index.html)
* Android Support Libraries
* [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html)
* [Koin](https://github.com/InsertKoinIO/koin)
* [Glide](https://github.com/bumptech/glide)
* [Retrofit](http://square.github.io/retrofit/)
* [OkHttp](http://square.github.io/okhttp/)
* [Moshi](https://github.com/square/moshi)
* [Timber](https://github.com/JakeWharton/timber)
* [Lottie](https://airbnb.design/lottie/)

## Requisitos

* JDK 1.8
* [Android SDK](https://developer.android.com/studio/index.html)
* Android O ([API 28](https://developer.android.com/preview/api-overview.html))
* Última versão Android SDK Tools e build tools.

## Arquitetura

A arquitetura do projeto segue os princípios do Clean Architecture de maneira simplificada para o teste. Veja na figura:

![Arquitetura](https://github.com/bufferapp/android-clean-architecture-boilerplate/blob/master/art/architecture.png?raw=true)

## Contatos

* Cândido Sales Gomes - [@candidosales](https://twitter.com/candidosales)